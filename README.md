# Panda E-Commerce

A simple E Commerce Website

## [Live Application](https://srabon444.gitlab.io/panda-commerce-bootstrap)


## Features

- UI shows various types of products

## Tech Stack

- Bootstrap
- HTML
- CSS

**Hosting:**
- Gitlab Pages


